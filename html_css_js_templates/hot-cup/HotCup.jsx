const HotCup = () => {
    return (
        <div>
            <div className="container">
                <div className="plate"></div>
                <div className="cup">
                    <div className="top">
                        <div className="vapour">
                            <span style = {{ "--my-css-var": 1 }}></span>
                            <span style = {{ "--my-css-var": 3 }}></span>
                            <span style = {{ "--my-css-var": 16 }}></span>
                            <span style = {{ "--my-css-var": 5 }}></span>
                            <span style = {{ "--my-css-var": 13 }}></span>
                            <span style = {{ "--my-css-var": 20 }}></span>
                            <span style = {{ "--my-css-var": 6 }}></span>
                            <span style = {{ "--my-css-var": 7 }}></span>
                            <span style = {{ "--my-css-var": 10 }}></span>
                            <span style = {{ "--my-css-var": 8 }}></span>
                            <span style = {{ "--my-css-var": 17 }}></span>
                            <span style = {{ "--my-css-var": 11 }}></span>
                            <span style = {{ "--my-css-var": 12 }}></span>
                            <span style = {{ "--my-css-var": 14 }}></span>
                            <span style = {{ "--my-css-var": 2 }}></span>
                            <span style = {{ "--my-css-var": 9 }}></span>
                            <span style = {{ "--my-css-var": 15 }}></span>
                            <span style = {{ "--my-css-var": 4 }}></span>
                            <span style = {{ "--my-css-var": 19 }}></span>
                        </div>
                        <div className="circle">
                            <div className="coffee"></div>
                        </div>
                    </div>
                    <div className="handle"></div>
                </div>
            </div>
        </div>
    );
};

export default HotCup;
